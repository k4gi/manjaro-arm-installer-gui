#! /bin/python

## Author: Dan Johansen <strit@manjaro.org> ##

import sys
import subprocess
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QAction, QLineEdit, QMessageBox, QLabel, QComboBox, QFileDialog
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import re

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'Manjaro ARM Installer GUI'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
        
        
    def initUI(self):
        self.setWindowTitle(self.title)
        #self.setWindowIcon(QIcon('icon.png')) #Does not work on wayland it seems
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage('Version 0.1 Alpha')
        
        # Labels
        username = QLabel('Username:', self)
        username.move(20, 20)
        password = QLabel('Password:', self)
        password.move(20, 60)
        rootpassword = QLabel('Root Password:', self)
        rootpassword.move(20, 100)
        device = QLabel('Device:', self)
        device.move(20, 140)
        edition = QLabel('Edition:', self)
        edition.move(20, 180)
        target = QLabel('SD/eMMC Card:', self)
        target.move(20, 220)
        locale = QLabel('Locale:', self)
        locale.move(20, 260)
        keyboard = QLabel('Key Layout:', self)
        keyboard.move(20, 300)
        hostname = QLabel('Hostname:', self)
        hostname.move(20, 340)
        
        ## Text input field
        # Username
        self.username = QLineEdit(self)
        self.username.move(150, 20)
        self.username.resize(280, 30)
        # Password
        self.password = QLineEdit(self)
        self.password.move(150, 60)
        self.password.resize(280, 30)
        # Root Password
        self.rootpassword = QLineEdit(self)
        self.rootpassword.move(150, 100)
        self.rootpassword.resize(280, 30)
        # Hostname
        self.hostname = QLineEdit(self)
        self.hostname.move(150, 340)
        self.hostname.resize(280, 30)
        
        ## Combo boxes
        # Device
        self.devicebox = QComboBox(self)
        self.devicebox.addItem('Raspberry Pi 4', 'rpi4')
        self.devicebox.addItem('Pinebook Pro', 'pbpro')
        self.devicebox.addItem('Rock Pi 4', 'rockpi4')
        self.devicebox.addItem('RockPro64', 'rockpro64')
        self.devicebox.move(150, 140)
        self.devicebox.resize(125, 30)
        # Edition
        self.editionbox = QComboBox(self)
        self.editionbox.addItem('Xfce', 'xfce')
        self.editionbox.addItem('KDE Plasma', 'kde-plasma')
        self.editionbox.addItem('Minimal', 'minimal')
        self.editionbox.addItem('Mate', 'mate')
        self.editionbox.move(150, 180)
        self.editionbox.resize(125, 30)
        # Target device SD or eMMC
        self.targetbox = QComboBox(self)
        self.targetbox.addItem('sda', 'sda')
        self.targetbox.addItem('mmcblk', 'mmcblk')
        self.targetbox.move(150, 220)
        self.targetbox.resize(125, 30)
        # Locale
        self.localebox = QComboBox(self)
        self.localebox.addItem('Dansk', 'da_DK.UTF-8')
        self.localebox.addItem('English (US)', 'en_US.UTF-8')
        self.localebox.addItem('English (UK)', 'en_GB.UTF-8')
        self.localebox.move(150, 260)
        self.localebox.resize(125, 30)
        # Keybaord layout
        self.keyboardbox = QComboBox(self)
        self.keyboardbox.addItem('Dansk', 'dk')
        self.keyboardbox.addItem('English (US)', 'us')
        self.keyboardbox.addItem('English (UK)', 'gb')
        self.keyboardbox.move(150, 300)
        self.keyboardbox.resize(125, 30)
        
        # Button in the window
        buttonClose = QPushButton('Close', self)
        buttonClose.setToolTip('Closes the Installer script and echos the variables in terminal!')
        buttonClose.move(100, 400)
        buttonClose.clicked.connect(self.on_click_close)

        # Button to test input
        buttonTest = QPushButton('Test', self)
        buttonTest.setToolTip('Tests input field variables for validity')
        buttonTest.move(200,400)
        buttonTest.clicked.connect(self.on_click_test)
        self.show()

    def on_click_test(self):
        usernameValue = self.username.text()
        passwordValue = self.password.text()
        rootpasswordValue = self.rootpassword.text()
        hostnameValue = self.hostname.text()
        if re.match('^[a-z]+$', usernameValue):
            print('Username OK (only lowercase)')
        else:
            print('Username not OK (blank, or contains uppercase or symbols)')
        if hostnameValue != '':
            print('Hostname OK (not blank)')
        else:
            print('Hostname not OK (blank)')


    @pyqtSlot()
    def on_click_close(self):
        usernameValue = self.username.text()
        passwordValue = self.password.text()
        rootpasswordValue = self.rootpassword.text()
        deviceValue = self.devicebox.currentData()
        editionValue = self.editionbox.currentData()
        targetValue = self.targetbox.currentData()
        localeValue = self.localebox.currentData()
        keyboardValue = self.keyboardbox.currentData()
        hostnameValue = self.hostname.text()
        subprocess.Popen(["echo", f"Username = {usernameValue}"])
        subprocess.Popen(["echo", f"Password for {usernameValue} = {passwordValue}"])
        subprocess.Popen(["echo", f"Password for root = {rootpasswordValue}"])
        subprocess.Popen(["echo", f"Device = {deviceValue}"])
        subprocess.Popen(["echo", f"Edition = {editionValue}"])
        subprocess.Popen(["echo", f"SD/eMMC = {targetValue}"])
        subprocess.Popen(["echo", f"Locale = {localeValue}"])
        subprocess.Popen(["echo", f"Keyboard Layout = {keyboardValue}"])
        subprocess.Popen(["echo", f"Hostname = {hostnameValue}"])
        sys.exit()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_()) 
